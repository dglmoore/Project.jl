module Generate

export project, experiment

import Base.LibGit2
importall Base.LibGit2
import ..Project: ProjectError

const remotes = Dict(:github => "github.com",
                     :gitlab => "gitlab.com",
                     :bitbucket => "bitbucket.org")

today() = Dates.format(Dates.now(), "yyyy-mm-dd")
author_name(repo) = LibGit2.getconfig("user.name", "")
remote_user(host=:github) = LibGit2.getconfig("$host.user", "")

function project(project;
                 host=:github,
                 author="",
                 chdir=true,
                 config=Dict(),
                 force=false)
    root = replace(lowercase(project), " ", "-")
    user = remote_user(host)
    remote = get(remotes, host, "$host.com")

    isnew = !ispath(root)
    try
        repo = if isnew
            url = isempty(user) ? "" : "https://$remote/$user/$root.git"
            Generate.init(root, url, config=config)
        else
            repo = GitRepo(root)
            if LibGit2.isdirty(repo)
                finalize(repo)
                throw(ProjectError("$root is dirty – commit or stash your changes"))
            end
            repo
        end

        LibGit2.transact(repo) do repo
            if isempty(author)
                author = author_name(repo)
            end

            files = [Generate.project_readme(project, root, author, force),
                     Generate.worklog(project, root, author, force),
                     Generate.gitignore(root, force),
                     Generate.experiments(root, force)...]

            map!(f -> relpath(f, root), files, files)

            msg = "$root $(isnew ? "generated" : "regenerated") files"
            LibGit2.add!(repo, files..., flags = LibGit2.Consts.INDEX_ADD_FORCE)
            if isnew
                info("Committing $root generated files")
                LibGit2.commit(repo, msg)
            elseif LibGit2.isdirty(repo)
                LibGit2.remove!(repo, files...)
                info("Regenerated files left unstaged, use `git add -p` to select")
                open(io->print(io,msg), joinpath(LibGit2.gitdir(repo),"MERGE_MSG"), "w")
            else
                info("Regenerated files are unchanged")
            end
        end

    catch
        isnew && rm(root, recursive=true)
        rethrow()
    end

    if (chdir)
        cd(root)
    end

    return root
end

function init(root, url; config::Dict=Dict())
    if !ispath(root)
        info("Initializing $root repo")
        repo = LibGit2.init(root)
        try
            with(GitConfig, repo) do cfg
                for (key,val) in config
                    LibGit2.set!(cfg, key, val)
                end
            end
            LibGit2.commit(repo, "initial empty commit")
        catch err
            throw(ProjectError("Unable to initialize $root project: $err"))
        end
    else
        repo = GitRepo(root)
    end

    try
        if !isempty(url)
            info("Origin: $url")
            with(LibGit2.GitRemote, repo, "origin", url) do rmt
                LibGit2.save(rmt)
            end
            LibGit2.set_remote_url(repo, url)
        end
    end
    return repo
end

function genfile(f::Function, root, file, force=false, append=false)
    path = joinpath(root, file)
    mode = append ? "a" : "w"
    if force || !ispath(path)
        info("Generating $path")
        mkpath(dirname(path))
        open(f, path, mode)
        return path
    end
    return ""
end

function project_readme(project, root, author, force=false)
    genfile(root, "README.adoc", force) do io
        print(io,
              """
              = $project
              $author
              :toc2:
              :source-highlighter: prettify
              :stem:

              == Introduction

              == Results

              == Discussion

              == Conclusion

              """)
    end
end

function worklog(project, root, author, force=false)
    genfile(root, "WORKLOG.adoc", force) do io
        print(io,
              """
              = Worklog
              :source-highlighter: prettify
              :stem:

              * $(Generate.today()) Create the "$project" project
              """)
    end
end

gitignore(root, force=false) = genfile(root, ".gitignore", force) do io
    print(io, """
          *.jl.cov
          *.jl.*.cov
          *.jl.mem
          *.html
          """)
end

function experiments(root, force=false)
    path = joinpath(root, "experiments")

    experiments_readme(path, force),
    gitignore(joinpath(path, "src"), force),
    gitignore(joinpath(path, "data"), force)
end

function experiments_readme(root, force=false)
    genfile(root, "README.adoc", force) do io
        print(io, """
              = Experiments
              :toc2:
              :source-highlighter: prettify
              :stem:

              :leveloffset: 1
              """)
    end
end

function experiment_id()
    root = "experiments"
    date = Dates.format(now(), "yyyymmdd")
    regex = Regex("^($date)([a-z])\\s*-\\s*(.+)\$")
    exps  = filter(f -> ismatch(regex, f), readdir(root))

    if isempty(exps)
        "$(date)a"
    else
        m = sort(map(s -> match(regex, s)[2], exps))[end][end] + 1
        date * string(m)
    end
end

function experiment(name;
                    chdir=true,
                    force=false)
    id = experiment_id()
    root = "experiments"
    experiment = "$(id)-$(replace(lowercase(name),' ','-'))"
    path = joinpath(root, experiment)

    experiment_readme(name, root, experiment, force)

    experiment_code(name, root, experiment, force)

    gitignore(joinpath(root, experiment, "data"), force)

    genfile(".", "WORKLOG.adoc", true, true) do io
        println(io, "* $(Generate.today()) Create the \"$name\" experiment")
    end

    if (chdir)
        cd(path)
    end

    return path
end

function experiment_code(name, root, experiment, force=false)
    path = joinpath(root, experiment)

    genfile(joinpath(path, "src"), "main.jl", force) do io
        print(io,
              """
              function main()
                  println("Running experiment \\"$name\\"")
              end
              """)
    end

    genfile(path, "runall.jl", force) do io
        print(io,
              """
              # This script will run all scripts in the experiment
              include("$(joinpath("src","main.jl"))")

              main()
              """)
    end
end

function experiment_readme(name, root, experiment, force=false)
    path = joinpath(root, experiment)

    genfile(path, "README.adoc", force) do io
        print(io,
              """
              = $name
              :toc2:
              :source-highlighter: prettify
              :stem:

              == Introduction

              == Results

              == Conclusions

              """)
    end

    genfile(root, "README.adoc", true, true) do io
        println(io, "include::$(joinpath(experiment, "README.adoc"))[]")
    end
end

end
