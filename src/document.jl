module Document

export document

const adoc = "asciidoctor"
const adoc_backend = "html5"
const adoc_attrs = ["stem"]
const adoc_regex = r"^.*\.adoc$"

function adoc_output(input)
    if !ismatch(adoc_regex, input)
        input * ".html"
    else
        base, _ = splitext(input)
        base * ".html"
    end
end

function adoc_command(input, output)
    attrs = join(("-a $a" for a in adoc_attrs), ' ')
    `$adoc -b $adoc_backend $attrs -o $output $input`
end

function make_doc(input)
    output = adoc_output(input)
    info("Making $output")
    run(adoc_command(input, output))
end

function clean_doc(input)
    output = adoc_output(input)
    info("Cleaning $output")
    rm(output, force=true)
end

process_docs(func) = process_docs(".", func)

function process_docs(root, func)
    for (root, _, files) in walkdir(root)
        for file in files
            if ismatch(adoc_regex, file)
                func(joinpath(root, file))
            end
        end
    end
end

make_docs() = process_docs(make_doc)
clean_docs() = process_docs(clean_doc)

function document(targets...)
    const recipes = Dict("all" => make_docs, "clean" => clean_docs)

    if isempty(targets)
        make_docs()
    else
        for target in targets
            if haskey(recipes, target)
                recipes[target]()
            elseif isfile(target)
                make_doc(target)
            else
                error("unrecognized target \"$target\"")
            end
        end
    end
end

end
