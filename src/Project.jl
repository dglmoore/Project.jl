__precompile__(true)

module Project

export Document, Generate

struct ProjectError
    msg::AbstractString
end

include("document.jl")
include("generate.jl")

document = Project.Document.document

end
